package genericstask;

public class GenericTaskMain {

    public static void main(String[] args) {

        Sports volley = new Sports(new Voley("FC Viitorul"));
        Sports football = new Sports(new Fotball("FC Barcelona"));

        volley.addTeam(new Voley("FC Speranta"));
        volley.addTeam(new Voley("FC StarWars"));
        football.addTeam(new Fotball("FC Haiduc"));
        football.addTeam(new Fotball("FC Eclipsa"));

//      Example of requirement error: football.addTeam(new Volley("Example")); - compile time error

        volley.sort();
        football.sort();

        System.out.println("Voley league: " + volley);
        System.out.println("Fotball league: " + football);
    }
}
