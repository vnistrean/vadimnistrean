package genericstask;

public class Fotball implements Comparable<Fotball> {

    private String teamName;

    private String getTeamName() {
        return teamName;
    }

    public Fotball(String name) {
        this.teamName = name;
    }

    @Override
    public String toString() {
        return teamName;
    }

    public int compareTo(Fotball team) {
        return this.teamName.compareTo(team.getTeamName());
    }
}
