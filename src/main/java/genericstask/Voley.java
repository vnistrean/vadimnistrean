package genericstask;

public class Voley implements Comparable<Voley> {

    private String teamName;

    public Voley(String name) {
        this.teamName = name;
    }

    private String getTeamName() {
        return teamName;
    }

    @Override
    public String toString() {
        return teamName ;
    }

    public int compareTo(Voley team) {
        return this.teamName.compareTo(team.getTeamName());
    }
}
