package seleniumtask;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartPage {

    public ShoppingCartPage()
    { PageFactory.initElements(WebDriverSingletone.getDriver(), this);}

    @FindBy(xpath = "//ul[@class='list-inline']//i[@class='fa fa-shopping-cart']")
    public WebElement shoppingCartButton;
    @FindBy(xpath = "//*[@id=\"content\"]/form/div/table/tbody/tr/td[4]/div/span/button[2]")
    public WebElement deleteTheProduct;
    @FindBy(xpath = "//*[@id=\"cart\"]/button")
    public WebElement shoppingCartVerify;
    @FindBy(xpath = "//*[@id=\"cart\"]/ul/li/p")
    public WebElement emptyShoppingCart;
    @FindBy(linkText = "HTC Touch HD")
    public WebElement verifyProductIsDisplayed;
    @FindBy(linkText = "iPhone")
    public WebElement verifyProductTwoIsDisplayed;
}
