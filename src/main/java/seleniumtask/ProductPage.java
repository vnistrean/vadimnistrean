package seleniumtask;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage {
    public ProductPage()
    { PageFactory.initElements(WebDriverSingletone.getDriver(), this);}

    @FindBy(xpath = "//*[@id=\"menu\"]/div[2]/ul/li[6]/a")
    public WebElement productCategoryPage;
    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]/div/div[2]/div[2]/button[1]")
    public WebElement productAddToCart;
    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[2]/button[1]")
    public WebElement productTwoAddToCart;
}
