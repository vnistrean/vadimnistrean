package seleniumtask;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.logging.Logger;

import static java.time.Duration.ofMillis;

public class Actions {

    static Logger logger = Logger.getLogger(Actions.class.getName());

    public static void click(WebElement element) {
        org.openqa.selenium.support.ui.Wait<WebDriver> wait = new FluentWait<>(WebDriverSingletone.getDriver())
                .withTimeout(ofMillis(2000))
                .pollingEvery(ofMillis(2000))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);

        wait.until(ExpectedConditions.visibilityOf(element));
        logger.info("Clicked " + element.getText());
        element.click();
    }

    public static void sendKeys(WebElement element, String string) {
        org.openqa.selenium.support.ui.Wait<WebDriver> wait = new FluentWait<>(WebDriverSingletone.getDriver())
                .withTimeout(ofMillis(2000))
                .pollingEvery(ofMillis(2000))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);

        wait.until(ExpectedConditions.visibilityOf(element));
        logger.info("Text inserted" + element.getText());
        element.sendKeys(string);
    }

    public static void isDisplayed(WebElement element) {
        org.openqa.selenium.support.ui.Wait<WebDriver> wait = new FluentWait<>(WebDriverSingletone.getDriver())
                .withTimeout(ofMillis(2000))
                .pollingEvery(ofMillis(2000))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);

        wait.until(ExpectedConditions.visibilityOf(element));
        logger.info("Is Displayed " + element.getText());
        element.isDisplayed();
    }
}