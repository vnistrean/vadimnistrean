package seleniumtask;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    public LoginPage()
    { PageFactory.initElements(WebDriverSingletone.getDriver(), this);}

    @FindBy(xpath = "//*[@id=\"top-links\"]/ul/li[2]/a")
    public WebElement loginMenu;
    @FindBy(xpath = "//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")
    public WebElement loginPage;
    @FindBy(xpath = "//*[@id=\"input-email\"]")
    public WebElement userName;
    @FindBy(xpath = "//*[@id=\"input-password\"]")
    public WebElement password;
    @FindBy(xpath = "//*[@id=\"content\"]/div/div[2]/div/form/input")
    public WebElement logging;
}
