package junit;

public class Calculator {
    public Calculator() {
    }

    public double multiply(double firstNumber, double secondNumber){
        double genericValue = 0;
        if(firstNumber >= 0 && firstNumber <= 100 && secondNumber >= 0 && secondNumber <= 100){
            genericValue = firstNumber * secondNumber;
        } else if(firstNumber <0 && secondNumber <0){
            throw new IllegalArgumentException("Number must be greater or equal than 0");
        }else throw new IllegalArgumentException("Number must be less or equal than 100");
        return genericValue;
    }

    public int subtract(int firstNumber, int secondNumber){
        int genericValue = -1;
        if((secondNumber >= 0) && (firstNumber > secondNumber)){
            genericValue =  firstNumber - secondNumber;
        } else if(secondNumber <0){
            throw new IllegalArgumentException("Number must be greater or equal than 0");
        } else throw new IllegalArgumentException("First number must be greater than second number");
        return genericValue;
    }

    public double divide(double numberToBeDivided, double divisor){
        double genericValue = 0;
        if(numberToBeDivided > 0 && divisor > 0 && numberToBeDivided > divisor){
            genericValue =  numberToBeDivided / divisor;
        } else if(numberToBeDivided ==0 && divisor ==0) {
            throw new IllegalArgumentException("You can not divide by 0");
        }else throw new IllegalArgumentException("Divisor must be less than number to be divided");
        return genericValue;
    }
}