package junit;

public enum EnumValues {

    VALUE_ONE(-1,2),
    VALUE_TWO(-6,-2),
    VALUE_THREE(5,15);

    private int value;
    private int valueTwo;

    EnumValues(int firstNumber, int secondNumber) {
        this.value = firstNumber;
        this.valueTwo = secondNumber;
    }
    public int getValue() {
        return value;
    }
    public int getValueTwo() {
        return valueTwo;
    }
}