package iotask;
import java.io.File;
import java.io.IOException;

public class IOTaskMain {

    public static void main (String args[]) throws IOException {
        File sourceFile;
        File destinationFile;
        //First Sub-task: Method of read a file and output to console:
        MotoGarage.garage();
        System.out.println("-----------------------------------");
        //Second Sub-task: Method of read a file and output to console:
        MotoGarage.garageTwo();
        System.out.println("-----------------------------------");
        //Third Sub-task: Method of read a file and output to console:
        MotoGarage.garageThree();
        System.out.println("-----------------------------------");
        //Extra method with StringWriter
        MotoGarage.garageFour();
        //Fourth Sub-task: Method of transfer from one file to another
        sourceFile = new File("file.txt");
        destinationFile = new File("file2.txt");
        CopyFiles.copyFileUsingFiles(sourceFile,destinationFile);
        //Fifth Sub-task:Second method of transfer from one file to another
        // Precondition: Third Sub-task created file2, delete it!
        CopyFiles.copyFileUsingChannel(sourceFile,destinationFile);

    }
}
