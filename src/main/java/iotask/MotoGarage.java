package iotask;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MotoGarage {

    public static void garage() throws IOException {
        Path path = Paths.get("listOfMotorcycles.txt");
        List<String> contents = Files.readAllLines(path);
        for (String content : contents) {
            System.out.println(content);
        }
    }
    public static void garageTwo(){
        try{ DataInputStream listOfMoto = new DataInputStream (new FileInputStream("listOfMotorcycles.txt"));
            byte[] datainBytes = new byte[listOfMoto.available()];
            listOfMoto.readFully(datainBytes);
            listOfMoto.close();
            String content = new String(datainBytes, 0, datainBytes.length);
            System.out.println(content);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public static void garageThree() throws IOException {
        BufferedReader listOfMoto = new BufferedReader(new FileReader("listOfMotorcycles.txt"));
        StringBuilder stringFromFile = new StringBuilder();
        String line = listOfMoto.readLine();
        while(line != null){
            stringFromFile.append(line);
            stringFromFile.append(System.lineSeparator());
            line = listOfMoto.readLine();
        }
        String container = stringFromFile.toString();
        System.out.println(container);
    }
    public static void garageFour() throws IOException {
        char[] arrayForCharactersFromFile = new char[1000000];
        StringWriter writeListOfMoto = new StringWriter();
        FileInputStream inputOfFile = new FileInputStream("listOfMotorcycles.txt");
        BufferedReader buffer = new BufferedReader(new InputStreamReader(inputOfFile, "UTF-8"));
        int x;
        while ((x = buffer.read(arrayForCharactersFromFile)) != -1) {
            writeListOfMoto.write(arrayForCharactersFromFile, 0, x);
        }
        System.out.println(writeListOfMoto.toString());
        writeListOfMoto.close();
        buffer.close();
    }
}