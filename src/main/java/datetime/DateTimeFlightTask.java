package datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class DateTimeFlightTask {

    final static ZonedDateTime TIME_DEPARTED = ZonedDateTime.of(LocalDateTime.of(LocalDate.of(2019,11,13), LocalTime.of(11, 15)), ZoneId.of("Australia/Sydney"));

    final static ZonedDateTime TIME_ARRIVED = ZonedDateTime.of(LocalDateTime.of(LocalDate.of(2019,11,13), LocalTime.of(06, 00)), ZoneId.of("America/Los_Angeles"));

    public static void duration() {
        Duration durationOfFlight = Duration.between(TIME_DEPARTED, TIME_ARRIVED);
        System.out.println("Duration of flight: " + durationOfFlight.toMinutes()/60 + ":" + durationOfFlight.toMinutes()%60);
    }
    public static void timeDepartedString() {
        String timeDepartedStringFormat = TIME_DEPARTED.format(DateTimeFormatter.ofPattern("d-M-yyyy hh:mm a"));
        System.out.println("Local time departed from Sydney: " +timeDepartedStringFormat);
    }
    public static void timeArrivedString() {
        String timeArrivedStringFormat = TIME_ARRIVED.format(DateTimeFormatter.ofPattern("d-M-yyyy hh:mm a"));
        System.out.println("Local time arrived in Los Angeles: " + timeArrivedStringFormat);
    }

}
