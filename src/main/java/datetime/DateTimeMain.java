package datetime;

import java.text.ParseException;

public class DateTimeMain {

    public static void main(String[] args) throws ParseException {
        System.out.println("_______Task One_________");
        DateTimeFlightTask.timeDepartedString();
        DateTimeFlightTask.timeArrivedString();
        DateTimeFlightTask.duration();

        System.out.println("_______Task Two_________");
        DateTimeMovieTask.dayOfPremiere();
        DateTimeMovieTask.numberOfPlayedTimesTwoYears();
        DateTimeMovieTask.durationInFristYear();
    }
}