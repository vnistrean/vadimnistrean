package datetime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

public class DateTimeMovieTask {

    static final String INPUT_DATE="25/05/1977";

    public static void dayOfPremiere() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date dateTime = format.parse(INPUT_DATE);
        DateFormat format2=new SimpleDateFormat("EEEE");
        String day = format2.format(dateTime);
        System.out.println("The day of premiere was: " + day);
    }

    public static long numberOfWeekDaysFromPeriod(DayOfWeek aaa, LocalDate firstDate, LocalDate lastDate){
        LocalDate firstDayFromRange = firstDate.with(TemporalAdjusters.next(aaa));
        LocalDate lastDayFromRange = lastDate.with(TemporalAdjusters.next(aaa));
        long number = ChronoUnit.WEEKS.between(firstDayFromRange, lastDayFromRange);
        return number +1;
    }

    public static void numberOfPlayedTimesTwoYears() {
    int numberPlayedTimes = (int) (numberOfWeekDaysFromPeriod(DayOfWeek.SUNDAY, LocalDate.of(1977, 5, 25), LocalDate.of(1979, 5, 25))
            + numberOfWeekDaysFromPeriod(DayOfWeek.SATURDAY, LocalDate.of(1977, 5, 25), LocalDate.of(1979, 5, 25)) +1 );
    System.out.println("Number of played movie sessions in first two years: " + numberPlayedTimes);
    }

    public static void durationInFristYear() {
        int numberPlayedTimes = (int) (numberOfWeekDaysFromPeriod(DayOfWeek.SUNDAY, LocalDate.of(1977, 5, 25), LocalDate.of(1978, 5, 25))
                + numberOfWeekDaysFromPeriod(DayOfWeek.SATURDAY, LocalDate.of(1977, 5, 25), LocalDate.of(1978, 5, 25)) + 1);
        int totalOfMinutes = numberPlayedTimes*120;
        int totalOfHours = totalOfMinutes /60;
        int totalOfDays = totalOfHours / 24;
        System.out.println("Total duration of movie for first year: \n" + "Days: " +String.valueOf(totalOfDays)+ "\n" + "Hours: " + String.valueOf(totalOfHours) + "\n" +
                "Minutes: " + String.valueOf(totalOfMinutes));
    }
}










