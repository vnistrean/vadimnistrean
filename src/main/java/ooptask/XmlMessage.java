package ooptask;

public class XmlMessage implements MessageInterface {
    final static String name = "XmlMessageName";
    private String body;

    public String getBody() {
        return body;
    }

    public XmlMessage(){
        prepareMessage();
    }

    public XmlMessage(String body) {
        prepareMessage(body);
    }

    public XmlMessage(String bodyTwo, String bodyThree){
        prepareMessage(bodyTwo,bodyThree);
    }

    @Override
    public String toString() {
        return name + ", XmlMessageBody= '" + body + '\'' ;
    }

    @Override
    public void prepareMessage() {
        this.body = ("Empty XML Message");
    }

    public void prepareMessage(String string) {
        this.body = string;
    }

    public void prepareMessage(String string1, String string2) {
        this.body = string1 + ", " + string2;
    }

    public Object getBodyFromName(String name){
        if(name == XmlMessage.name){
           return new XmlMessage(getBody());
        }return new Exception("No message found with such Message Name");
    }


}
