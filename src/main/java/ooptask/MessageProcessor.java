package ooptask;

public class MessageProcessor {

    public static void main(String[] args) {

        MessageConvertor messageConvertor = new MessageConvertor();

        XmlMessage messageOne = new XmlMessage();
        XmlMessage messageTwo = new XmlMessage("XML body one");
        XmlMessage messageThree = new XmlMessage("XML body two", "XML body three");
        JsonMessage messageFour = new JsonMessage();
        JsonMessage messageFive = new JsonMessage("Json body one");
        JsonMessage messageSix = new JsonMessage("Json body two", "Json body three");

        System.out.println("Below you can see body of message by inserting message name: ");
        System.out.println(messageOne.getBodyFromName("XmlMessageName"));
        System.out.println(messageFive.getBodyFromName("JsonMessageName"));
        //Error message if message name is not founded or is incorrect:
        System.out.println(messageOne.getBodyFromName("NotExistingName"));

        System.out.println("Below you can see all created Messages converted one to another: ");
        System.out.println(messageConvertor.convert(ConvertionType.XML, messageFour.getBody()));
        System.out.println(messageConvertor.convert(ConvertionType.XML, messageFive.getBody()));
        System.out.println(messageConvertor.convert(ConvertionType.XML, messageSix.getBody()));
        System.out.println(messageConvertor.convert(ConvertionType.JSON, messageOne.getBody()));
        System.out.println(messageConvertor.convert(ConvertionType.JSON, messageTwo.getBody()));
        System.out.println(messageConvertor.convert(ConvertionType.JSON, messageThree.getBody()));
    }



}
