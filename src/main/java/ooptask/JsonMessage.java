package ooptask;

public class JsonMessage implements MessageInterface  {
    final static String name = "JsonMessageName";
    private String body;

    public String getBody() {
        return body;
    }

    public JsonMessage(){
        prepareMessage();
    }

    public JsonMessage(String body) {
        prepareMessage(body);
    }

    public JsonMessage(String bodyTwo, String bodyThree){
        prepareMessage(bodyTwo, bodyThree);
    }

    @Override
    public String toString() {
        return  name + ", JsonMessageBody= '" + body + '\'' ;
    }

    @Override
    public void prepareMessage() {
        this.body = ("Empty Json Message");
    }

    public void prepareMessage(String string) {
        this.body = string;
    }

    public void prepareMessage(String string1, String string2) {
        this.body = string1 + ", " + string2;
    }

    public Object getBodyFromName(String name){
        if(name == JsonMessage.name){
            return new JsonMessage(getBody());
        }return new Exception("No message found with such Message Name");
    }
}
