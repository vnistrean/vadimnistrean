package ooptask;

public interface MessageInterface {
    void prepareMessage();
    public String getBody();
}
