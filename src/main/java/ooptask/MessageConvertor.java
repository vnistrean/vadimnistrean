package ooptask;

public class MessageConvertor {

    public Object convert(ConvertionType convertTo, String body) {
        switch (convertTo) {
            case XML:
                return new XmlMessage(body);
            case JSON:
                return new JsonMessage(body);
            default:
                return new Exception("no such conversion type");
        }
    }
}