package charlist;

import java.util.*;
import java.util.stream.Collectors;

public class CharList {

    private List<Character> list;

    private CharList(ArrayList<Character> list){
        this.list = list;
    }

    protected CharList(String s){
        char[] chars = s.toCharArray();
        list = new ArrayList<>();
        for (int i = 0; i < chars.length; i++) {
            list.add(chars[i]);
        }
    }

    @Override
    public String toString() {
        return list.stream().map(i -> i.toString())
                .collect(Collectors.joining("|"));
    }

    protected int length(){
        return list.size();
    }

    protected int indexOf(char characterToCheck){
        return list.indexOf(characterToCheck);
    }

    protected char charAt(int index) {
        if ((index < 0) || (index >= list.size())) {
            throw new StringIndexOutOfBoundsException(index);
        } return list.get(index);
    }

    protected CharList subString(int start, int end) {
        if (start < 0 || end > list.size() - 1 || end < start) {
            throw new IllegalArgumentException("start or\\and end arguments wrong ");
        } return new CharList(new ArrayList<>(list.subList(start, end)));
    }

    protected CharList removeDuplicates(){
        ArrayList<Character> containerDuplicates = new ArrayList<>(list);
        return new CharList(new ArrayList<>(containerDuplicates.stream().distinct().collect(Collectors.toList())));
    }

    protected CharList removeFirst(Character firstCharacter){
        ArrayList<Character> containerRemoveFirst = new ArrayList<>(list);
        containerRemoveFirst.remove(firstCharacter);
        return new CharList(new ArrayList<>(containerRemoveFirst));
    }

    protected CharList removeAll(Character character){
        ArrayList<Character> containerRemoveAll = new ArrayList<>(list);
        list.removeAll(Collections.singleton(character));
        return new CharList(new ArrayList<>(containerRemoveAll));
    }

    protected boolean isEmpty(){
        return list.isEmpty();
    }

    protected boolean contains(Character characterToBeChecked){
        return list.contains(characterToBeChecked);
    }

    protected CharList clearList(){
        list.clear();
        return new CharList(new ArrayList<>(list));
    }

    protected CharList sortList(){
        ArrayList<Character> containerSortList = new ArrayList<>(list);
        containerSortList.sort(Character::compareTo);
        return new CharList(new ArrayList<>(containerSortList));
    }

    protected CharList reverseList(){
        ArrayList<Character> containerReverse = new ArrayList<>(list);
        Collections.reverse(containerReverse);
        return new CharList(new ArrayList<>(containerReverse));
    }

    protected CharList mixedList(){
        ArrayList<Character> containerMix = new ArrayList<>(list);
        Collections.shuffle(containerMix);
        return new CharList(new ArrayList<>(containerMix));
    }
}