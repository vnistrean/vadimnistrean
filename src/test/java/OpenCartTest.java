
import org.junit.jupiter.api.Test;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import seleniumtask.LoginPage;
import seleniumtask.ProductPage;
import seleniumtask.ShoppingCartPage;
import seleniumtask.WebDriverSingletone;

import static seleniumtask.Actions.*;

public class OpenCartTest{

    public WebDriver driver = WebDriverSingletone.getDriver();
    public LoginPage login = new LoginPage();
    public ProductPage product = new ProductPage();
    public ShoppingCartPage shoppingCart = new ShoppingCartPage();
    final static String OPEN_CART_HOME_URL = "https://demo.opencart.com/";

    @Test
    public void OpenCartTestOneProduct() throws InterruptedException {
        driver.get(OPEN_CART_HOME_URL);
        click(login.loginMenu);
        click(login.loginPage);
        sendKeys(login.userName, "vn@gmail.com");
        sendKeys(login.password, "qwerty1");
        click(login.logging);
        click(product.productCategoryPage);
        click(product.productAddToCart);
        click(shoppingCart.shoppingCartButton);
        isDisplayed(shoppingCart.verifyProductIsDisplayed);
        click(shoppingCart.deleteTheProduct);
        Thread.sleep(1500);//Added this timer because without it, its assertion does not work properly
        click(shoppingCart.shoppingCartVerify);
        isDisplayed(shoppingCart.emptyShoppingCart);
    }

    @Test
    public void OpenCartTestTwoProducts() throws InterruptedException {
        driver.get(OPEN_CART_HOME_URL);
        click(login.loginMenu);
        click(login.loginPage);
        sendKeys(login.userName, "test@testvn.com");
        sendKeys(login.password, "1111");
        click(login.logging);
        click(product.productCategoryPage);
        click(product.productAddToCart);
        click(product.productTwoAddToCart);
        click(shoppingCart.shoppingCartButton);
        isDisplayed(shoppingCart.verifyProductIsDisplayed);
        isDisplayed(shoppingCart.verifyProductTwoIsDisplayed);
        click(shoppingCart.deleteTheProduct);
        Thread.sleep(1500);//Added this timer because without it, its assertion does not work properly
        try {
            isDisplayed(shoppingCart.verifyProductIsDisplayed);
        } catch (TimeoutException exception) {
            System.out.println("Product is removed from cart!");
        }
    }
}
