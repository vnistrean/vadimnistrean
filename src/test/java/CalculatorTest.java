import junit.Calculator;
import junit.EnumValues;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {

    private static long startTime;

    public Calculator calculator = new Calculator();

    public static Logger logger = Logger.getLogger(CalculatorTest.class.getName());

    private static Stream<Arguments> argumentsStreamForDivide(){
        return Stream.of(
                Arguments.of(36,6,"Divide"),
                Arguments.of(10,2,"Divide"),
                Arguments.of(15,5,"Divide")
        );
    }

    private static Stream<Arguments> argumentsStreamForNegativeTest(){
        return Stream.of(
                Arguments.of(-36,6,"Divide"),
                Arguments.of(5,35,"Divide"),
                Arguments.of(0,5,"Divide")
        );
    }

    private static Stream<Arguments> argumentsStreamForSubtract(){
        return Stream.of(
                Arguments.of(50,10,"Substract"),
                Arguments.of(10,5,"Substract"),
                Arguments.of(39,38,"Substract")
        );
    }

    @BeforeAll
    static void testSetUp(){
        startTime = System.currentTimeMillis();
    }

    @AfterAll
    static void tearDown(){
        float testRunDuration = (System.currentTimeMillis() - startTime)/10000f;
        logger.info("Test duration: " + testRunDuration + " seconds");
    }

    @CsvSource(value = {"10.5,0.0", "20.5,2.4", "6.5,3.5"})
    @DisplayName("Multiply positive test using Csv source")
    @ParameterizedTest(name = "Multiply between {0} and {1}")
    void multiplyPositiveTest(double firstNumber, double secondNumber){
        double expected = firstNumber * secondNumber;
        logger.info("Result of multiply between "+ firstNumber + " and " + secondNumber + " is: " + String.valueOf(expected));
        assertThat("Expected of multiply two numbers",calculator.multiply(firstNumber,secondNumber), is(expected));
    }

    @CsvSource(value = {"101,1", "-1,2", "-6,-2"})
    @DisplayName("Multiply negative test using Csv source")
    @ParameterizedTest(name = "Multiply between {0} and {1}")
    void multiplyNegatieTest(double firstNumber, double secondNumber){
        Executable executable = () -> calculator.multiply(firstNumber, secondNumber);
        assertThrows(Exception.class, executable, "Multiply negative");
    }

    @MethodSource("argumentsStreamForDivide")
    @DisplayName("Divide positive test using MethodSource")
    @ParameterizedTest(name = "{2} test on between {0} and {1}")
    void dividePositiveTest(Integer firstNumber, Integer secondNumber, String operation){
        double expectedDivide = firstNumber / secondNumber;
        logger.info("Result of divide between "+ firstNumber + " and " + secondNumber + " is: " + String.valueOf(expectedDivide));
        assertThat("Expected of dividing two numbers",calculator.divide(firstNumber,secondNumber), is(expectedDivide));
    }

    @MethodSource("argumentsStreamForNegativeTest")
    @DisplayName("Divide negative test using MethodSource")
    @ParameterizedTest(name = "Divide test on between {0} and {1}")
    void divideNegativeTest(Integer firstNumber, Integer secondNumber){
        Executable executable = () -> calculator.divide(firstNumber, secondNumber);
        assertThrows(Exception.class, executable, "Divide negative");
    }

    @MethodSource("argumentsStreamForSubtract")
    @DisplayName("Subtract positive test using MethodSource")
    @ParameterizedTest(name = "Subtract test on between {0} and {1}")
    void subtractPositiveTest(Integer firstNumber, Integer secondNumber, String operation){
        int expectedSubtract = firstNumber - secondNumber;
        logger.info("Result of subtract between "+ firstNumber + " and " + secondNumber + " is: " + String.valueOf(expectedSubtract));
        assertThat("Expected of subtract two numbers",calculator.subtract(firstNumber,secondNumber), is(expectedSubtract));
    }

    @ParameterizedTest(name = "[{index}] EnumValue {arguments}")
    @EnumSource(EnumValues.class)
    @DisplayName("Subtract negative test using EnumSource")
    void subtractNegativeTestTwo(EnumValues enumValues){
        Executable executable = () -> calculator.subtract(enumValues.getValue(),enumValues.getValueTwo());
        assertThrows(Exception.class, executable, "Subtract negative");
    }
}